package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Open_New_Account_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Type_of_Account")
	public static WebElement Type_of_Account;

public void verify_Type_of_Account(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Type_of_Account.getAttribute("value"),data);
	}

}

public void verify_Type_of_Account_Status(String data){
		//Verifies the Status of the Type_of_Account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Type_of_Account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Type_of_Account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Type_of_Account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Type_of_Account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Type_of_Account(String data){
		Select dropdown= new Select(Type_of_Account);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Existing_Account")
	public static WebElement Existing_Account;

public void verify_Existing_Account(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Existing_Account.getAttribute("value"),data);
	}

}

public void verify_Existing_Account_Status(String data){
		//Verifies the Status of the Existing_Account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Existing_Account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Existing_Account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Existing_Account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Existing_Account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void select_Existing_Account(String data){
		Select dropdown= new Select(Existing_Account);
		 dropdown.selectByVisibleText(data);
}

@FindBy(how= How.ID, using = "Open_New_Account")
	public static WebElement Open_New_Account;

public void verify_Open_New_Account_Status(String data){
		//Verifies the Status of the Open_New_Account
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Open_New_Account.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Open_New_Account.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Open_New_Account.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Open_New_Account.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Open_New_Account(){
		Open_New_Account.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}