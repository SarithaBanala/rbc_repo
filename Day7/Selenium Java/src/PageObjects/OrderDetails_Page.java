package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class OrderDetails_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "OrderInfo")
	public static WebElement OrderInfo;

public void verify_OrderInfo(String data){
		Assert.assertEquals(OrderInfo,OrderInfo);
}

public void verify_OrderInfo_Status(String data){
		//Verifies the Status of the OrderInfo
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(OrderInfo.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(OrderInfo.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!OrderInfo.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!OrderInfo.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}