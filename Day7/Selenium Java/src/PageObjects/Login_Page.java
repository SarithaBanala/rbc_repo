package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Login_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Username")
	public static WebElement Username;

public void verify_Username(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Username.getAttribute("value"),data);
	}

}

public void verify_Username_Status(String data){
		//Verifies the Status of the Username
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Username.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Username.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Username.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Username.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Username(String data){
		Username.clear();
		Username.sendKeys(data);
}

@FindBy(how= How.ID, using = "Password")
	public static WebElement Password;

public void verify_Password(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(Password.getAttribute("value"),data);
	}

}

public void verify_Password_Status(String data){
		//Verifies the Status of the Password
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Password.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Password.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Password.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Password.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_Password(String data){
		Password.clear();
		Password.sendKeys(data);
}

@FindBy(how= How.ID, using = "Login")
	public static WebElement Login;

public void verify_Login_Status(String data){
		//Verifies the Status of the Login
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Login.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Login.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Login.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Login.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Login(){
		Login.click();
}

@FindBy(how= How.ID, using = "Error_Message")
	public static WebElement Error_Message;

public void verify_Error_Message(String data){
		Assert.assertEquals(Error_Message,Error_Message);
}

public void verify_Error_Message_Status(String data){
		//Verifies the Status of the Error_Message
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Error_Message.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Error_Message.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Error_Message.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Error_Message.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}