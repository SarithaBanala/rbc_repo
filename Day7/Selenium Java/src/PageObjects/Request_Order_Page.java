package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Request_Order_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "OrderNumber")
	public static WebElement OrderNumber;

public void verify_OrderNumber(String data){
		if(!data.contentEquals("Dont care")){
		Assert.assertEquals(OrderNumber.getAttribute("value"),data);
	}

}

public void verify_OrderNumber_Status(String data){
		//Verifies the Status of the OrderNumber
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(OrderNumber.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(OrderNumber.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!OrderNumber.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!OrderNumber.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void set_OrderNumber(String data){
		OrderNumber.clear();
		OrderNumber.sendKeys(data);
}

@FindBy(how= How.ID, using = "1234OrderNumber")
	public static WebElement 1234OrderNumber;

public void verify_1234OrderNumber_Status(String data){
		//Verifies the Status of the 1234OrderNumber
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(1234OrderNumber.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(1234OrderNumber.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!1234OrderNumber.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!1234OrderNumber.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_1234OrderNumber(){
		1234OrderNumber.click();
}

@FindBy(how= How.ID, using = "Ok")
	public static WebElement Ok;

public void verify_Ok_Status(String data){
		//Verifies the Status of the Ok
		if(!data.contentEquals("Dont care")){
			switch(data){
			case "ENABLED":
				Assert.assertTrue(Ok.isEnabled());
				break;
			case "VISIBLE":
				Assert.assertTrue(Ok.isDisplayed());
				break;
			case "HIDDEN":
				Assert.assertFalse(!Ok.isDisplayed());
				break;
			case "DISABLED":
				Assert.assertFalse(!Ok.isEnabled());
				break;
			default:
				break;
			}
		}
	}
public void click_Ok(){
		Ok.click();
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}