package PageObjects;
import org.testng.Assert;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.ui.Select;
import utilities.WebController;
import utilities.PageObjectBase;
@SuppressWarnings("deprecation")
public class Req_to_validate_credentials_Page extends PageObjectBase{
@FindBy(how= How.ID, using = "Username")
	public static WebElement Username;

public void verify_Username(String data){
		Assert.assertEquals(Username,Username);
}

public void enter_Username(String data){
		Username.sendKeys(data);
}

@FindBy(how= How.ID, using = "Password")
	public static WebElement Password;

public void verify_Password(String data){
		Assert.assertEquals(Password,Password);
}

public void enter_Password(String data){
		Password.sendKeys(data);
}

public static void verify_Text(String data){
	Assert.assertFalse(driver.getPageSource().contains(data));
}
}