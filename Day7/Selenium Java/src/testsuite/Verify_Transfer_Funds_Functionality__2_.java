package testsuite;
import org.testng.annotations.Test;
import PageObjects.*;
import utilities.PageObjectBase;
import org.openqa.selenium.support.PageFactory;
import utilities.Configurations;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import java.util.HashMap;
import org.testng.annotations.AfterTest;
import org.testng.annotations.DataProvider;
import utilities.TestReport;
import java.io.IOException;
import org.testng.Reporter;
import utilities.DataUtil;


/** Conformiq generated test case
	Verify_Transfer_Funds_Functionality__2_
*/
public class Verify_Transfer_Funds_Functionality__2_ extends PageObjectBase
{

	public Verify_Transfer_Funds_Functionality__2_()
	{
	}

	private TestReport testReport= new TestReport();


	private StringBuilder overallTestData= new StringBuilder();


	@Test(dataProvider="TestData")
	public void test(final String Step_1_Username_TEXTBOX_Status,final String Step_1_Username_TEXTBOX_Verification,final String Step_1_Password_TEXTBOX_Status,final String Step_1_Password_TEXTBOX_Verification,final String Step_1_Login_BUTTON_Status,final String Step_1_Error_Message_LABEL_Status,final String Step_2_Username_TEXTBOX,final String Step_2_Password_TEXTBOX,final String Step_4_Open_New_Account_HYPERLINK_Status,final String Step_4_Transfer_Funds_HYPERLINK_Status,final String Step_4_Logout_HYPERLINK_Status,final String Step_6_Amount_TEXTBOX_Status,final String Step_6_Amount_TEXTBOX_Verification,final String Step_6_From_Account_DROPDOWN_Status,final String Step_6_From_Account_DROPDOWN_Verification,final String Step_6_To_Account_DROPDOWN_Status,final String Step_6_To_Account_DROPDOWN_Verification,final String Step_6_Transfer_BUTTON_Status,final String Step_7_Amount_TEXTBOX,final String Step_7_From_Account_DROPDOWN,final String Step_7_To_Account_DROPDOWN,final String Step_10_Username_TEXTBOX_Status,final String Step_10_Username_TEXTBOX_Verification,final String Step_10_Password_TEXTBOX_Status,final String Step_10_Password_TEXTBOX_Verification,final String Step_10_Login_BUTTON_Status,final String Step_10_Error_Message_LABEL_Status) throws Exception

	{

	Login_Page login_page_init=PageFactory.initElements(driver, Login_Page.class);

	Account_Services_Page account_services_page_init=PageFactory.initElements(driver, Account_Services_Page.class);

	Open_New_Account_Page open_new_account_page_init=PageFactory.initElements(driver, Open_New_Account_Page.class);

	Transfer_Funds_Page transfer_funds_page_init=PageFactory.initElements(driver, Transfer_Funds_Page.class);

	Req_to_validate_credentials_Page req_to_validate_credentials_page_init=PageFactory.initElements(driver, Req_to_validate_credentials_Page.class);

	Response_Page response_page_init=PageFactory.initElements(driver, Response_Page.class);

	Request_Order_Page request_order_page_init=PageFactory.initElements(driver, Request_Order_Page.class);

	OrderDetails_Page orderdetails_page_init=PageFactory.initElements(driver, OrderDetails_Page.class);
	testReport.createTesthtmlHeader(overallTestData);

	testReport.createHead(overallTestData);

	testReport.putLogo(overallTestData);

	float ne = (float) 0.0;

	testReport.generateGeneralInfo(overallTestData, "Verify_Transfer_Funds_Functionality__2_", "TC_Verify_Transfer_Funds_Functionality__2_", "",ne);

	testReport.createStepHeader();

	//External Circumstances


	Reporter.log("Step - 1- verify Login screen");

	testReport.fillTableStep("Step 1", "verify Login screen");

	login_page_init.verify_Username_Status(Step_1_Username_TEXTBOX_Status);

	login_page_init.verify_Username(Step_1_Username_TEXTBOX_Verification);
	login_page_init.verify_Password_Status(Step_1_Password_TEXTBOX_Status);

	login_page_init.verify_Password(Step_1_Password_TEXTBOX_Verification);
	login_page_init.verify_Login_Status(Step_1_Login_BUTTON_Status);

	login_page_init.verify_Error_Message_Status(Step_1_Error_Message_LABEL_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_1");

	Reporter.log("Step - 2- Fill Customer Login form Login screen");

	testReport.fillTableStep("Step 2", "Fill Customer Login form Login screen");

	login_page_init.set_Username(Step_2_Username_TEXTBOX);
	login_page_init.set_Password(Step_2_Password_TEXTBOX);
	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_2");

	Reporter.log("Step - 3- click Login button Login screen");

	testReport.fillTableStep("Step 3", "click Login button Login screen");

	login_page_init.click_Login();
	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_3");

	Reporter.log("Step - 4- verify Account Services screen");

	testReport.fillTableStep("Step 4", "verify Account Services screen");

	account_services_page_init.verify_Open_New_Account_Status(Step_4_Open_New_Account_HYPERLINK_Status);

	account_services_page_init.verify_Transfer_Funds_Status(Step_4_Transfer_Funds_HYPERLINK_Status);

	account_services_page_init.verify_Logout_Status(Step_4_Logout_HYPERLINK_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_4");

	Reporter.log("Step - 5- click Transfer Funds hyperlink Account Services screen");

	testReport.fillTableStep("Step 5", "click Transfer Funds hyperlink Account Services screen");

	account_services_page_init.click_Transfer_Funds();
	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_5");

	Reporter.log("Step - 6- verify Transfer Funds screen");

	testReport.fillTableStep("Step 6", "verify Transfer Funds screen");

	transfer_funds_page_init.verify_Amount_Status(Step_6_Amount_TEXTBOX_Status);

	transfer_funds_page_init.verify_Amount(Step_6_Amount_TEXTBOX_Verification);
	transfer_funds_page_init.verify_From_Account_Status(Step_6_From_Account_DROPDOWN_Status);

	transfer_funds_page_init.verify_From_Account(Step_6_From_Account_DROPDOWN_Verification);
	transfer_funds_page_init.verify_To_Account_Status(Step_6_To_Account_DROPDOWN_Status);

	transfer_funds_page_init.verify_To_Account(Step_6_To_Account_DROPDOWN_Verification);
	transfer_funds_page_init.verify_Transfer_Status(Step_6_Transfer_BUTTON_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_6");

	Reporter.log("Step - 7- Fill Transfer Funds form Transfer Funds screen");

	testReport.fillTableStep("Step 7", "Fill Transfer Funds form Transfer Funds screen");

	transfer_funds_page_init.set_Amount(Step_7_Amount_TEXTBOX);
	transfer_funds_page_init.select_From_Account(Step_7_From_Account_DROPDOWN);
	transfer_funds_page_init.select_To_Account(Step_7_To_Account_DROPDOWN);
	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_7");

	Reporter.log("Step - 8- click Transfer button Transfer Funds screen");

	testReport.fillTableStep("Step 8", "click Transfer button Transfer Funds screen");

	transfer_funds_page_init.click_Transfer();
	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_8");

	Reporter.log("Step - 9- click Logout hyperlink Account Services screen");

	testReport.fillTableStep("Step 9", "click Logout hyperlink Account Services screen");

	account_services_page_init.click_Logout();
	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_9");

	Reporter.log("Step - 10- verify Login screen");

	testReport.fillTableStep("Step 10", "verify Login screen");

	login_page_init.verify_Username_Status(Step_10_Username_TEXTBOX_Status);

	login_page_init.verify_Username(Step_10_Username_TEXTBOX_Verification);
	login_page_init.verify_Password_Status(Step_10_Password_TEXTBOX_Status);

	login_page_init.verify_Password(Step_10_Password_TEXTBOX_Verification);
	login_page_init.verify_Login_Status(Step_10_Login_BUTTON_Status);

	login_page_init.verify_Error_Message_Status(Step_10_Error_Message_LABEL_Status);

	getScreenshot(driver,Configurations.screenshotLocation , "Verify_Transfer_Funds_Functionality__2_","Step_10");
	}
	@DataProvider(name = "TestData")
	public Object[][] getData() {
	return DataUtil.getDataFromSpreadSheet("TestData.xlsx", "TCID_3");
}
	@AfterTest
	public void export(){
		testReport.appendtestData(overallTestData);
		testReport.closeStepTable();
		testReport.closeTestHTML(overallTestData);
		driver.close();
		try {
			testReport.writeTestReporthtml(overallTestData, "Verify_Transfer_Funds_Functionality__2_");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
