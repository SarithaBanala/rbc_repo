com.conformiq.creator.structure.v15
creator.gui.screen qml038843be7d09455892f4ae93498b94c8 "Login"
{
	creator.gui.form qml6efadcff08b14531bb29ae3036d903aa "Customer Login"
	{
		creator.gui.textbox qml82581bfe247541fa81420ba64e4a2401 "Username"
			type = String
			status = dontcare;
		creator.gui.textbox qml06b06d4ff4d9476086976f3b751a8785 "Password"
			type = String
			status = dontcare;
	}
	creator.gui.button qmlae36ece1e87b403ab7bee7ba56b41d4d "Login"
		status = dontcare;
	creator.gui.labelwidget qml31e65f8e2dbf497b90d825eebb707800 "Error Message"
		status = dontcare;
}
creator.gui.screen qml0670acef3fa24504a739e4e2a65b132b "Account Services"
{
	creator.gui.hyperlink qmlb9893c979de448168d2a487d3eb9a4df "Open New Account"
		status = dontcare;
	creator.gui.hyperlink qmlebf608b463d34d49b0e8d5ae0509e219 "Transfer Funds"
		status = dontcare;
	creator.gui.hyperlink qmldd7c4ffd785144bfbb8ce5b1dc4062a4 "Logout"
		status = dontcare;
}
creator.gui.screen qmla2727bb230c442f085feff064721c146 "Open New Account"
{
	creator.gui.form qml3735ecaab47946d592d05b0178f6bec8 "Open New Account"
	{
		creator.gui.dropdown qmld3a3f85693bb4ca484e31afb296197fa "Type of Account"
			type = qml66117a6b6166449490b8fe945faf1227
			status = dontcare;
		creator.gui.dropdown qml46fb260255a94411a6da5c04d95c864b "Existing Account"
			type = qml9439aa96311d42c5afb63343ff121e9b
			status = dontcare;
	}
	creator.gui.button qml530533e20d4c4def8079f9d3fb7be3c4 "Open New Account"
		status = dontcare;
}
creator.enum qml66117a6b6166449490b8fe945faf1227 "Account Type"
{
	creator.enumerationvalue qmleec0fa8045a4490e882b8621ad8d2e8c "Checking";
	creator.enumerationvalue qml1af23351ce5f4966957cc80f12d1d354 "Savings";
}
creator.enum qml9439aa96311d42c5afb63343ff121e9b "Existing Account"
{
	creator.enumerationvalue qml61433364cdad4de0b59c6c0b9948b15f "12343";
}
creator.gui.screen qmlaf7d6c5daf2b482aba114af0a0e31389 "Transfer Funds"
{
	creator.gui.form qml4b8ec3479a3941b19beda3bb91d9c2a8 "Transfer Funds"
	{
		creator.gui.textbox qmlf56684c8a6234e3fb44649218ec8fc63 "Amount"
			type = number
			status = dontcare;
		creator.gui.dropdown qmlaabf422b51e449ddbf3b1551c2a0e28f "From Account"
			type = qml29ae03bf4ba64f4ea831da3358f9e32a
			status = dontcare;
		creator.gui.dropdown qml00cfff28bfb4454091982d5fd75eb67b "To Account"
			type = qml92f5e0297dca4328803133eed04ecd70
			status = dontcare;
	}
	creator.gui.button qml59751c36830d49c88c450b281f1b6334 "Transfer"
		status = dontcare;
}
creator.enum qml29ae03bf4ba64f4ea831da3358f9e32a "From Account"
{
	creator.enumerationvalue qml077dbfc35c8f47fbb86aeacfbade9b93 "2232";
}
creator.enum qml92f5e0297dca4328803133eed04ecd70 "To Account"
{
	creator.enumerationvalue qml4ed76c5ea73f451896bf3b425bbb5637 "1365";
}
creator.message qml3c295f5b5cb84d149d557241c7c46f79
"Req to validate credentials"
	interfaces = [ qml149474a930c44ecb995ad5782772be46 ]
{
	creator.primitivefield qml4dc8562f88ac4b87b33fd8a3776b9c17 "Username"
		type = String;
	creator.primitivefield qmlcd43282038bc436cacfdaf24c77d7ad0 "Password"
		type = String;
}
creator.externalinterface qml149474a930c44ecb995ad5782772be46 "User1"
	direction = in;
creator.message qmlc6039893145d40f9b47ed5697813402a "Response"
	interfaces = [ qmle410924c66e845c3a1b7e0f5c0170245 ]
{
	creator.primitivefield qml587f0bb0eac1409c885813bece6dc7c2 "Status"
		type = String;
}
creator.externalinterface qmle410924c66e845c3a1b7e0f5c0170245 "Output"
	direction = out;
creator.gui.screen qml757fe59f4d174868ad697ab7f575f024 "Request Order"
{
	creator.gui.form qmlcac17080969141c6ac8511243cde3503 "Order Details"
	{
		creator.gui.textbox qmla6bf1d7315a4466d80ecfab92cdaa15b "OrderNumber"
			type = String
			status = dontcare;
	}
	creator.gui.hyperlink qmlfa1f14163473445caa6e271cd2c2989a "1234OrderNumber"
		status = dontcare;
	creator.gui.button qmlf559307b5b1e43e59a2b035d2182f768 "Ok"
		status = dontcare;
}
creator.gui.screen qml9c6402008135469e8bd2aa0dbd17cbff "OrderDetails"
{
	creator.gui.labelwidget qml35b569ca5e3a412da1e63bc3c3216231 "OrderInfo"
		status = dontcare;
}